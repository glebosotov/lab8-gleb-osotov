import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

service = Service(executable_path="~/Desktop/chromedriver")
driver = webdriver.Chrome()
driver.implicitly_wait(5)

# Create a new instance of the Chrome driver

# Navigate to the crates.io website
driver.get("https://crates.io/")

# Test Case 1: Search Functionality
# Find the search bar element on the page and enter a search term
search_bar = driver.find_element(By.NAME, "q")
search_bar.send_keys("rust web framework")

# Press the "Enter" key to perform the search
search_bar.send_keys(Keys.RETURN)

# Wait for the search results page to load and verify that it contains relevant crates
time.sleep(3)
search_results = driver.find_elements(By.CSS_SELECTOR, "[class^='_crate-row_']")
assert len(search_results) > 0, "Search functionality not working correctly"

# Test Case 2: Copy Code Button
# Navigate to a crate information page
driver.get("https://crates.io/crates/actix-web")

# Find the "Copy" button and click it
copy_button = driver.find_element(By.CSS_SELECTOR, "button[class*='_copy-button_']")
copy_button.click()

# Wait for the code to be copied and paste it into a text editor
time.sleep(1)
import pyperclip

assert pyperclip.paste() == "cargo add actix-web", "Copy code button not working correctly"

# Test Case 3: Focus on Search Field
# Press the 'S' key to focus on the search field
search_bar = driver.find_element(By.NAME, "q")
search_bar.send_keys(Keys.SHIFT, 's')

# Verify that the search bar is focused and the cursor is in the search field
time.sleep(1)
assert driver.switch_to.active_element == search_bar, "Pressing 'S' key not focusing on search field"

# Close the browser window
driver.quit()
