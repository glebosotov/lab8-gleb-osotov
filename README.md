# [crates.io](https://crates.io)

## Test cases

Test Case 1: Search Functionality

Description: This test case aims to verify that the search functionality of crates.io works correctly.
Precondition: The browser is open and on the crates.io website.
Test Steps:
Step 1: Find the search bar element on the page.
Step 2: Enter a search term, such as "rust web framework", into the search bar.
Step 3: Press the "Enter" key or click the search button to perform the search.
Step 4: Verify that the search results page is displayed and contains relevant crates matching the search term.
Expected Result: The search functionality should work correctly, and relevant crates should be displayed on the search results page.
Test Case 2: Copy Code Button

Description: This test case aims to verify that the "Copy" button on the crate information page works correctly and copies the crate code to the clipboard.
Precondition: The browser is open and on the crates.io website, and a crate information page is loaded.
Test Steps:
Step 1: Locate the "Copy" button on the crate information page.
Step 2: Click the "Copy" button to copy the crate code to the clipboard.
Step 3: Open a text editor or terminal and paste the copied code.
Step 4: Verify that the copied code matches the crate code displayed on the page.
Expected Result: The "Copy" button should work correctly and copy the crate code to the clipboard accurately.
Test Case 3: Focus on Search Field

Description: This test case aims to verify that pressing the 'S' key on the crates.io website focuses on the search field.
Precondition: The browser is open and on the crates.io website.
Test Steps:
Step 1: Press the 'S' key on the keyboard.
Step 2: Verify that the search bar is focused, and the cursor is in the search field.
Expected Result: Pressing the 'S' key should focus on the search field, and the cursor should be in the search bar.

## Test Results

All test cases passed successfully.

I am not able to run the tests in CI without gui setup, so I have run the tests locally and attached the gif of the test results.

![Test Results](./tests.gif)
